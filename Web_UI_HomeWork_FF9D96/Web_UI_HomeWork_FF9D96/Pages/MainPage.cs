﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web_UI_HomeWork_FF9D96.Widgets;

namespace Web_UI_HomeWork_FF9D96.Pages
{
    class MainPage : BasePage
    {
        static string WEB_PAGE = "https://liquipedia.net/";
        public MainPage(IWebDriver webDriver) : base(webDriver) { }
       
        public static MainPage Navigate(IWebDriver webDriver)
        {
        
            webDriver.Navigate().GoToUrl(WEB_PAGE);
            return new MainPage(webDriver);
        }

        public ActionWidget GetActionWidget()
        {
            return new ActionWidget(Driver);
        }
        public ActionResultWidget GetActionResultWidget()
        {
            return new ActionResultWidget(Driver);
        }
    }
}
