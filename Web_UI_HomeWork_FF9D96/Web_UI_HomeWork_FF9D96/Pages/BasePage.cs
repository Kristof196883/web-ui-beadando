﻿using OpenQA.Selenium;

namespace Web_UI_HomeWork_FF9D96.Pages
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
