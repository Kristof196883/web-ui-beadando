﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web_UI_HomeWork_FF9D96.Pages;

namespace Web_UI_HomeWork_FF9D96.Widgets
{
    class ActionWidget : BasePage
    {
        public ActionWidget(IWebDriver webDriver) : base(webDriver) { }

        public MainPage MakeAction(string action)
        {
            Driver.FindElement(By.Name("search")).SendKeys(action);
            Driver.FindElement(By.Name("search")).SendKeys(Keys.Enter);
            return new MainPage(Driver);
        }
    }
}
