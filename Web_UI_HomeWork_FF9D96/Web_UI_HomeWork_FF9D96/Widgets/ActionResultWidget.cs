﻿using OpenQA.Selenium;
using Web_UI_HomeWork_FF9D96.Pages;

namespace Web_UI_HomeWork_FF9D96.Widgets
{
    class ActionResultWidget : BasePage
    {
        public ActionResultWidget(IWebDriver webDriver) : base(webDriver) { }

        public string GetActionResult()
        {
            return Driver.FindElement(By.ClassName("firstHeading")).Text;
        }
    }
}
