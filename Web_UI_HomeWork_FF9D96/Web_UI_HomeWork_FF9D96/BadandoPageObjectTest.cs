﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using Web_UI_HomeWork_FF9D96.Pages;

namespace Web_UI_HomeWork_FF9D96
{
    class BadandoPageObjectTest : TestBase
    {

        [Test, TestCaseSource("TestData")]
        public void TestMain(string action, string result)
        {
            var content = MainPage.Navigate(Driver)
                .GetActionWidget()
                .MakeAction(action)
                .GetActionResultWidget()
                .GetActionResult();
            Assert.That(content, Is.EqualTo(result));
        }
        static IEnumerable TestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\Data.xml");
            return
                from vars in doc.Descendants("testData")
                let action = vars.Attribute("action").Value
                let result = vars.Attribute("result").Value
                select new object[] { action, result };
        }
    }
}
